#include <stdio.h>
int main(){
    char ch;
    printf("input a character : ");
    scanf("%c",&ch);
    if((ch < 'A') || (ch > 'z')){
        printf("\nInvalid Number\n");
        return 0;
    }

    if((ch=='a') || (ch=='e') || (ch=='i') || (ch=='o') || (ch=='u') || (ch=='A') || (ch=='E') || (ch=='I') || (ch=='O') || (ch=='U')){
        printf("\nVowel Letter\n");
    }else{
        printf("\nConsonant\n");
    }
    return 0;
}
